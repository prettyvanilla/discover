# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-17 02:32+0000\n"
"PO-Revision-Date: 2023-03-04 13:28+0100\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.2.2\n"

#: discover/DiscoverObject.cpp:159
#, kde-format
msgid ""
"Discover currently cannot be used to install any apps or perform system "
"updates because none of its app backends are available."
msgstr ""
"ამჟამად Discover -ის გამოყენება აპლიკაციების დასაყენებლად ან სისტემის "
"განახლებისთვის შეუძლებელია, რადგან მისი უკანაბოლოები ხელმისაწვდომი არაა."

#: discover/DiscoverObject.cpp:163
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can install some on the Settings page, under the <interface>Missing "
"Backends</interface> section.<nl/><nl/>Also please consider reporting this "
"as a packaging issue to your distribution."
msgstr ""
"ზოგიერთი შეგიძლიათ პარამეტრების გვერდიდან დააყენოთ,<interface>ნაკლული "
"უკანაბოლოების</interface> სექციიდან. <nl/><nl/>ასევე, ალბათ, დაგჭირდებათ "
"თქვენს დისტრიბუტივის მომწოდებელს პაკეტების პრობლემის შესახებ შეატყობინოთ."

#: discover/DiscoverObject.cpp:168 discover/DiscoverObject.cpp:380
#: discover/qml/UpdatesPage.qml:107
#, kde-format
msgid "Report This Issue"
msgstr "ამ პრობლემის რეპორტი"

#: discover/DiscoverObject.cpp:173
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can use <command>pacman</command> to install the optional dependencies "
"that are needed to enable the application backends.<nl/><nl/>Please note "
"that Arch Linux developers recommend using <command>pacman</command> for "
"managing software because the PackageKit backend is not well-integrated on "
"Arch Linux."
msgstr ""
"აპლიკაციის უკანაბოლოების ჩასართავად ბრძანება <command>pacman</command> "
"შეგიძლიათ გამოიყენოთ. <nl/><nl/>დაიმახსოვრეთ, რომ Arch Linux-ის "
"პროგრამისტები პროგრამების სამართავად გირჩევენ <command>pacman</command>-ი "
"გამოიყენოთ, რადგან უკანაბოლო PackageKit -ი Arch Linux-ში მთლად კარგად "
"ინტეგრირებულიც არაა."

#: discover/DiscoverObject.cpp:181
#, kde-format
msgid "Learn More"
msgstr "დაწვრილებით"

#: discover/DiscoverObject.cpp:269
#, kde-format
msgid "Could not find category '%1'"
msgstr "კატეგორია \"%1\" ვერ ვიპოვე"

#: discover/DiscoverObject.cpp:284
#, kde-format
msgid "Trying to open inexisting file '%1'"
msgstr "არარსებული ფაილის (%1) გახსნის მცდელობა"

#: discover/DiscoverObject.cpp:306
#, kde-format
msgid ""
"Cannot interact with flatpak resources without the flatpak backend %1. "
"Please install it first."
msgstr ""
"Flatpack-ის რესურსებთან Flatpack-ის უკანაბოლო %1-ის გარეშე ურთიერთობა "
"შეუძლებელია. ჯერ ის დააყენეთ."

#: discover/DiscoverObject.cpp:310
#, kde-format
msgid "Could not open %1"
msgstr "%1-ის გახსნა შეუძლებელია"

#: discover/DiscoverObject.cpp:372
#, kde-format
msgid "Please make sure Snap support is installed"
msgstr "დარწმუნდით, რომ Snap-ის მხარდაჭერა დაყენებულია"

#: discover/DiscoverObject.cpp:374
#, kde-format
msgid ""
"Could not open %1 because it was not found in any available software "
"repositories."
msgstr "%1-ის გასნა შეუძლებელია. ის არც ერთი რეპოზიტორიიდანაა ხელმისაწვდომი."

#: discover/DiscoverObject.cpp:377
#, kde-format
msgid "Please report this issue to the packagers of your distribution."
msgstr "შეატყობინეთ ეს შეცდომა თქვენს დისტრიბუტორს."

#: discover/DiscoverObject.cpp:442 discover/DiscoverObject.cpp:443
#: discover/main.cpp:120 discover/qml/BrowsingPage.qml:20
#, kde-format
msgid "Discover"
msgstr "აღმოაჩინეთ"

#: discover/DiscoverObject.cpp:443
#, kde-format
msgid ""
"Discover was closed before certain tasks were done, waiting for it to finish."
msgstr ""
"Discover დაიხურა ზოგიერთი ამოცანის დამთავრებამდე. ველოდები მათ დასრულებას."

#: discover/main.cpp:42
#, kde-format
msgid "Directly open the specified application by its appstream:// URI."
msgstr "მითითებული აპლიკაციის პირდაპირ გახსნა appstream:// URI-ის გამოყენებით."

#: discover/main.cpp:43
#, kde-format
msgid "Open with a search for programs that can deal with the given mimetype."
msgstr ""
"ისეთი პროგრამების მოძებნა, რომელსაც მითითებულ mimetype-ზე მუშაობა შეუძლიათ."

#: discover/main.cpp:44
#, kde-format
msgid "Display a list of entries with a category."
msgstr "კატეგორიის მქონე ჩანაწერების სიის ჩვენება."

#: discover/main.cpp:45
#, kde-format
msgid "Open Discover in a said mode. Modes correspond to the toolbar buttons."
msgstr "გახსენით Discover მითითებულ რეჟიმში. რეჟიმები ღილაკებს შეესაბამება."

#: discover/main.cpp:46
#, kde-format
msgid "List all the available modes."
msgstr "ხელმისაწვდომი რეჟიმების სია."

#: discover/main.cpp:47
#, kde-format
msgid "Compact Mode (auto/compact/full)."
msgstr ""
"კომპაქტური რეჟიმი (auto (ავტომატური)/compact (კომპაქტური)/full (სრული))."

#: discover/main.cpp:48
#, kde-format
msgid "Local package file to install"
msgstr "ლოკალური პაკეტი დასაყენებლად"

#: discover/main.cpp:49
#, kde-format
msgid "List all the available backends."
msgstr "ხელმისაწვდომი უკანაბოლოების სია."

#: discover/main.cpp:50
#, kde-format
msgid "Search string."
msgstr "საძებნი სტრიქონი."

#: discover/main.cpp:51
#, kde-format
msgid "Lists the available options for user feedback"
msgstr "მომხმარებლის უკუკავშირის ხელმისაწვდომი ვარიანტები"

#: discover/main.cpp:53
#, kde-format
msgid "Supports appstream: url scheme"
msgstr "აქვს აღმავალის მხარდაჭერა: url scheme"

#: discover/main.cpp:122
#, kde-format
msgid "An application explorer"
msgstr "აპლიკაციების ბრაუზერი"

#: discover/main.cpp:124
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2022 Plasma -ის პროგრამისტების გუნდი"

#: discover/main.cpp:125
#, kde-format
msgid "Aleix Pol Gonzalez"
msgstr "Aleix Pol Gonzalez"

#: discover/main.cpp:126
#, kde-format
msgid "Nate Graham"
msgstr "Nate Graham"

#: discover/main.cpp:127
#, kde-format
msgid "Quality Assurance, Design and Usability"
msgstr "დიზაინი და ხარისხის დამოწმება"

#: discover/main.cpp:131
#, kde-format
msgid "Dan Leinir Turthra Jensen"
msgstr "დან ლაინირ თუთა იენსენი"

#: discover/main.cpp:132
#, kde-format
msgid "KNewStuff"
msgstr "KNewStuff"

#: discover/main.cpp:139
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Temuri Doghonadze"

#: discover/main.cpp:139
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "Temuri.doghonadze@gmail.com"

#: discover/main.cpp:152
#, kde-format
msgid "Available backends:\n"
msgstr "ხელმისაწვდომი უკანაბოლოები:\n"

#: discover/main.cpp:208
#, kde-format
msgid "Available modes:\n"
msgstr "ხელმისაწვდომი რეჟიმები:\n"

#: discover/qml/AddonsView.qml:19 discover/qml/navigation.js:43
#, kde-format
msgid "Addons for %1"
msgstr "დამატებები %1-სთვის"

#: discover/qml/AddonsView.qml:51
#, kde-format
msgid "More…"
msgstr "მეტი…"

#: discover/qml/AddonsView.qml:60
#, kde-format
msgid "Apply Changes"
msgstr "ცვლილებების გადატარება"

#: discover/qml/AddonsView.qml:67
#, kde-format
msgid "Reset"
msgstr "საწყისი პარამეტრები"

#: discover/qml/AddSourceDialog.qml:20
#, kde-format
msgid "Add New %1 Repository"
msgstr "ახალი %1-ის რეპოზიტორიის დამატება"

#: discover/qml/AddSourceDialog.qml:44
#, kde-format
msgid "Add"
msgstr "დამატება"

#: discover/qml/AddSourceDialog.qml:49 discover/qml/DiscoverWindow.qml:269
#: discover/qml/InstallApplicationButton.qml:47
#: discover/qml/ProgressView.qml:109 discover/qml/SourcesPage.qml:190
#: discover/qml/UpdatesPage.qml:252 discover/qml/WebflowDialog.qml:41
#, kde-format
msgid "Cancel"
msgstr "გაუქმება"

#: discover/qml/ApplicationDelegate.qml:141
#: discover/qml/ApplicationPage.qml:214
#, kde-format
msgid "%1 rating"
msgid_plural "%1 ratings"
msgstr[0] "%1 შეფასება"
msgstr[1] "%1 შეფასება"

#: discover/qml/ApplicationDelegate.qml:141
#: discover/qml/ApplicationPage.qml:214
#, kde-format
msgid "No ratings yet"
msgstr "შეფასების გარეშე"

#: discover/qml/ApplicationPage.qml:65
#, kde-format
msgctxt ""
"@item:inlistbox %1 is the name of an app source e.g. \"Flathub\" or \"Ubuntu"
"\""
msgid "From %1"
msgstr "%1-დან"

#: discover/qml/ApplicationPage.qml:76
#, kde-format
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ApplicationPage.qml:194
#, kde-format
msgid "Unknown author"
msgstr "უცნობი შეცდომა"

#: discover/qml/ApplicationPage.qml:238
#, kde-format
msgid "Version:"
msgstr "ვერსია:"

#: discover/qml/ApplicationPage.qml:250
#, kde-format
msgid "Size:"
msgstr "ზომა:"

#: discover/qml/ApplicationPage.qml:262
#, kde-format
msgid "License:"
msgid_plural "Licenses:"
msgstr[0] "ლიცენზია:"
msgstr[1] "ლიცენზიები:"

#: discover/qml/ApplicationPage.qml:270
#, kde-format
msgctxt "The app does not provide any licenses"
msgid "Unknown"
msgstr "უცნობია"

#: discover/qml/ApplicationPage.qml:300
#, kde-format
msgid "What does this mean?"
msgstr "რას ნიშნავს?"

#: discover/qml/ApplicationPage.qml:309
#, kde-format
msgid "See more…"
msgid_plural "See more…"
msgstr[0] "იხილეთ მეტი…"
msgstr[1] "იხილეთ მეტი…"

#: discover/qml/ApplicationPage.qml:320
#, kde-format
msgid "Content Rating:"
msgstr "შემცველობის შეფასება:"

#: discover/qml/ApplicationPage.qml:329
#, kde-format
msgid "Age: %1+"
msgstr "ასაკი: %1+"

#: discover/qml/ApplicationPage.qml:353
#, kde-format
msgctxt "@action"
msgid "See details…"
msgstr "დეტალების ჩვენება…"

#: discover/qml/ApplicationPage.qml:375
#, kde-format
msgid "Could not access the screenshots"
msgstr "ეკრანის ანაბეჭდებთან წვდომა აკრძალულია"

#: discover/qml/ApplicationPage.qml:487
#, kde-format
msgid "Documentation"
msgstr "დოკუმენტაცია"

#: discover/qml/ApplicationPage.qml:488
#, kde-format
msgid "Read the project's official documentation"
msgstr "წაიკითხეთ პროექტის ოფიციალური დოკუმენტაცია"

#: discover/qml/ApplicationPage.qml:504
#, kde-format
msgid "Website"
msgstr "ვებგვერდი"

#: discover/qml/ApplicationPage.qml:505
#, kde-format
msgid "Visit the project's website"
msgstr "პროექტის ვებგვერდის ნახვა"

#: discover/qml/ApplicationPage.qml:521
#, kde-format
msgid "Addons"
msgstr "დამატებები"

#: discover/qml/ApplicationPage.qml:522
#, kde-format
msgid "Install or remove additional functionality"
msgstr "დამატებითი ფუნქციონალის დამატება ან წაშლა"

#: discover/qml/ApplicationPage.qml:541
#, kde-format
msgctxt "Exports the application's URL to an external service"
msgid "Share"
msgstr "გაზიარება"

#: discover/qml/ApplicationPage.qml:542
#, kde-format
msgid "Send a link for this application"
msgstr "ამ აპლიკაციის ბმულის გაგზავნა"

#: discover/qml/ApplicationPage.qml:558
#, kde-format
msgctxt "The subject line for an email. %1 is the name of an application"
msgid "Check out the %1 app!"
msgstr "იხილეთ %1 აპი!"

#: discover/qml/ApplicationPage.qml:578
#, kde-format
msgid "What's New"
msgstr "რა არის ახალი"

#: discover/qml/ApplicationPage.qml:609
#, kde-format
msgid "Loading reviews for %1"
msgstr "%1-ის შეფასებების ჩატვირთვა"

#: discover/qml/ApplicationPage.qml:615
#, kde-format
msgid "Reviews"
msgstr "მიმოხილვები"

#: discover/qml/ApplicationPage.qml:653
#, kde-format
msgid "Show all %1 Reviews"
msgid_plural "Show all %1 Reviews"
msgstr[0] "ყველა %1 მიმოხილვის ჩვენება"
msgstr[1] "ყველა %1 მიმოხილვის ჩვენება"

#: discover/qml/ApplicationPage.qml:665
#, kde-format
msgid "Write a Review"
msgstr "შეფასების დაწერა"

#: discover/qml/ApplicationPage.qml:665
#, kde-format
msgid "Install to Write a Review"
msgstr "მიმოხილვის დასაწერად დააყენეთ"

#: discover/qml/ApplicationPage.qml:677
#, kde-format
msgid "Get Involved"
msgstr "შემოგვიერთდით"

#: discover/qml/ApplicationPage.qml:719
#, kde-format
msgid "Donate"
msgstr "შემოწირულობა"

#: discover/qml/ApplicationPage.qml:720
#, kde-format
msgid "Support and thank the developers by donating to their project"
msgstr ""
"დაეხმარეთ და გადაუხადეთ პროგრამისტებს მადლობა მათი პროექტების მხარდაჭერით"

#: discover/qml/ApplicationPage.qml:736
#, kde-format
msgid "Report Bug"
msgstr "შეცდომის ანგარიში"

#: discover/qml/ApplicationPage.qml:737
#, kde-format
msgid "Log an issue you found to help get it fixed"
msgstr "ჩაიწერეთ ჟურნალში აღმოჩენილი შეცდომა, რათა ის გასწორდეს"

#: discover/qml/ApplicationPage.qml:753
#, kde-format
msgid "Contribute"
msgstr "შემოწირულობა"

#: discover/qml/ApplicationPage.qml:754
#, kde-format
msgid "Help the developers by coding, designing, testing, or translating"
msgstr "დაეხმარეთ პროგრამისტებს კოდით, დიზაინით, ტესტირებით ან თარგმნით"

#: discover/qml/ApplicationPage.qml:779
#, kde-format
msgid "All Licenses"
msgstr "ყველა ლიცენზია"

#: discover/qml/ApplicationPage.qml:812
#, kde-format
msgid "Content Rating"
msgstr "შემცველობის შეფასება"

#: discover/qml/ApplicationPage.qml:826
#, kde-format
msgid "Risks of proprietary software"
msgstr "დახურული კოდის მქონე პროგრამების რისკი"

#: discover/qml/ApplicationPage.qml:832
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (<link url='%1'>%2</link>).<nl/><nl/>You can "
"learn more at <link url='%3'>%3</link>."
msgstr ""
"ამ აპლიკაციის საწყისი კოდი ნაწილობრივ ან სრულად დახურულია საჯარო შემოწმებისა "
"და გაუმჯობესებისათვის. ეს ნიშნავს, რომ მესამე პირებს და მომხმარებლებს "
"(მაგალითად, თქვენ) არ შეუძლიათ გადაამოწმონ მისი ოპერაციები, უსაფრთხოება და "
"სანდოობა. ან შეცვალონ და გადასცენ სხვას ავტორის ნებართვის გარეშე.<nl/><nl/"
">აპლიკაცია შეიძლება ძალიანაც უსაფრთხო იყოს, მაგრამ ასევე შეიძლება უცნაურად "
"იქცეოდეს - მაგალითად მოაგროვოს ინფორმაცია თქვენს შესახებ, თვალყური ადევნოს "
"თქვენს მდებარეობას ან თქვენი ფაილები სხვას გადაუგზავნოს. ადვილი საშუალება, "
"გაიგოთ ეს, არ არსებობს. ამიტომ მხოლოდ სანდო ავტორების პროგრამები უნდა "
"დააყენოთ (<link url='%1'>%2</link>).<nl/><nl/>გაიგეთ მეტი <link url='%3'>%3</"
"link>."

#: discover/qml/ApplicationPage.qml:833
#, kde-kuit-format
msgctxt "@info"
msgid ""
"This application's source code is partially or entirely closed to public "
"inspection and improvement. That means third parties and users like you "
"cannot verify its operation, security, and trustworthiness, or modify and "
"redistribute it without the authors' express permission.<nl/><nl/>The "
"application may be perfectly safe to use, or it may be acting against you in "
"various ways—such as harvesting your personal information, tracking your "
"location, or transmitting the contents of your files to someone else. There "
"is no easy way to be sure, so you should only install this application if "
"you fully trust its authors (%1).<nl/><nl/>You can learn more at <link "
"url='%2'>%2</link>."
msgstr ""
"ამ აპლიკაციის საწყისი კოდი ნაწილობრივ ან სრულად დახურულია საჯარო შემოწმებისა "
"და გაუმჯობესებისათვის. ეს ნიშნავს, რომ მესამე პირებს და მომხმარებლებს "
"(მაგალითად, თქვენ) არ შეუძლიათ გადაამოწმონ მისი ოპერაციები, უსაფრთხოება და "
"სანდოობა. ან შეცვალონ და გადასცენ სხვას ავტორის ნებართვის გარეშე.<nl/><nl/"
">აპლიკაცია შეიძლება ძალიანაც უსაფრთხო იყოს, მაგრამ ასევე შეიძლება უცნაურად "
"იქცეოდეს - მაგალითად მოაგროვოს ინფორმაცია თქვენს შესახებ, თვალყური ადევნოს "
"თქვენს მდებარეობას ან თქვენი ფაილები სხვას გადაუგზავნოს. ადვილი საშუალება, "
"გაიგოთ ეს, არ არსებობს. ამიტომ მხოლოდ სანდო ავტორების პროგრამები უნდა "
"დააყენოთ (%1).<nl/><nl/>გაიგეთ მეტი <link url='%2'>%2</link>."

#: discover/qml/ApplicationsListPage.qml:54
#, kde-format
msgid "Search: %2 - %3 item"
msgid_plural "Search: %2 - %3 items"
msgstr[0] "ძებნა: %2 - %3 ჩანაწერი"
msgstr[1] "ძებნა: %2 - %3 ჩანაწერი"

#: discover/qml/ApplicationsListPage.qml:56
#, kde-format
msgid "Search: %1"
msgstr "ძებნა: %1"

#: discover/qml/ApplicationsListPage.qml:60
#, kde-format
msgid "%2 - %1 item"
msgid_plural "%2 - %1 items"
msgstr[0] "%1 - %2 ჩანაწერი"
msgstr[1] "%1 - %2 ჩანაწერი"

#: discover/qml/ApplicationsListPage.qml:66
#, kde-format
msgid "Search - %1 item"
msgid_plural "Search - %1 items"
msgstr[0] "ძებნა - %1 ჩანაწერი"
msgstr[1] "ძებნა - %1 ჩანაწერი"

#: discover/qml/ApplicationsListPage.qml:68
#: discover/qml/ApplicationsListPage.qml:226
#, kde-format
msgid "Search"
msgstr "ძებნა"

#: discover/qml/ApplicationsListPage.qml:89
#, kde-format
msgid "Sort: %1"
msgstr "დალაგება: %1"

#: discover/qml/ApplicationsListPage.qml:92
#, kde-format
msgid "Name"
msgstr "სახელით"

#: discover/qml/ApplicationsListPage.qml:101
#, kde-format
msgid "Rating"
msgstr "შეფასება"

#: discover/qml/ApplicationsListPage.qml:110
#, kde-format
msgid "Size"
msgstr "ზომა"

#: discover/qml/ApplicationsListPage.qml:119
#, kde-format
msgid "Release Date"
msgstr "რელიზის თარიღი"

#: discover/qml/ApplicationsListPage.qml:174
#, kde-format
msgid "Nothing found"
msgstr "ვერაფერი ვიპოვე"

#: discover/qml/ApplicationsListPage.qml:181
#, kde-format
msgctxt "@action:button"
msgid "Search in All Categories"
msgstr "ყველა კატეგორიაში ძებნა"

#: discover/qml/ApplicationsListPage.qml:190
#, kde-format
msgctxt "@action:button %1 is the name of an application"
msgid "Search the Web for \"%1\""
msgstr "\"%1\"-ის ვებში ძებნა"

#: discover/qml/ApplicationsListPage.qml:194
#, kde-format
msgctxt ""
"If appropriate, localize this URL to be something more relevant to the "
"language. %1 is the text that will be searched for."
msgid "https://duckduckgo.com/?q=%1"
msgstr "https://duckduckgo.com/?q=%1"

#: discover/qml/ApplicationsListPage.qml:205
#, kde-format
msgctxt ""
"@info:placeholder %1 is the name of an application; %2 is the name of a "
"category of apps or add-ons"
msgid "\"%1\" was not found in the \"%2\" category"
msgstr "\"%1\" კატეგორიაში \"%2\" აღმოჩენილი არაა"

#: discover/qml/ApplicationsListPage.qml:207
#, kde-format
msgctxt "@info:placeholder %1 is the name of an application"
msgid "\"%1\" was not found in the available sources"
msgstr "\"%1\" ხელმისაწვდომ წყაროებში აღმოჩენილი არაა"

#: discover/qml/ApplicationsListPage.qml:208
#, kde-format
msgctxt "@info:placeholder%1 is the name of an application"
msgid ""
"\"%1\" may be available on the web. Software acquired from the web has not "
"been reviewed by your distributor for functionality or stability. Use with "
"caution."
msgstr ""
"\"%1\" შეიძლება ინტერნეტში იყოს ხელმისაწვდომი. ინტერნეტიდან გადმოწერილი "
"პროგრამები არაა შემოწმებული თქვენი დისტრიბუტორის მიერ. ფრთხილად გამოიყენეთ "
"ის."

#: discover/qml/ApplicationsListPage.qml:241
#, kde-format
msgid "Still looking…"
msgstr "ჯერ კიდევ ვეძებ…"

#: discover/qml/BrowsingPage.qml:50
#, kde-format
msgid "Unable to load applications"
msgstr "აპლიკაციების ჩატვირთვის შეცდომა"

#: discover/qml/BrowsingPage.qml:89
#, kde-format
msgctxt "@title:group"
msgid "Most Popular"
msgstr "პოპულარული"

#: discover/qml/BrowsingPage.qml:107
#, kde-format
msgctxt "@title:group"
msgid "Editor's Choice"
msgstr "რედაქტორის არჩევანი"

#: discover/qml/BrowsingPage.qml:121
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Games"
msgstr "უმაღლესი-რეიტინგის თამაშები"

#: discover/qml/BrowsingPage.qml:140 discover/qml/BrowsingPage.qml:169
#, kde-format
msgctxt "@action:button"
msgid "See More"
msgstr "იხილეთ მეტი"

#: discover/qml/BrowsingPage.qml:150
#, kde-format
msgctxt "@title:group"
msgid "Highest-Rated Developer Tools"
msgstr "უმაღლესი-რეიტინგის პროგრამები პროგრამისტებისთვის"

#: discover/qml/DiscoverWindow.qml:44
#, kde-format
msgid "Running as <em>root</em> is discouraged and unnecessary."
msgstr "<em>root</em> -ით გაშვება რეკომენდებული და საჭირო არაა."

#: discover/qml/DiscoverWindow.qml:57
#, kde-format
msgid "&Home"
msgstr "&სახლი"

#: discover/qml/DiscoverWindow.qml:67
#, kde-format
msgid "&Search"
msgstr "&ძებნა"

#: discover/qml/DiscoverWindow.qml:75
#, kde-format
msgid "&Installed"
msgstr "&დაყენებული"

#: discover/qml/DiscoverWindow.qml:87
#, kde-format
msgid "Fetching &updates…"
msgstr "&განახლებების გამოთხოვა…"

#: discover/qml/DiscoverWindow.qml:87
#, kde-format
msgid "&Up to date"
msgstr "&განახლებულია"

#: discover/qml/DiscoverWindow.qml:88
#, kde-format
msgctxt "Update section name"
msgid "&Update (%1)"
msgstr "&განახლება (%1)"

#: discover/qml/DiscoverWindow.qml:96
#, kde-format
msgid "&About"
msgstr "შ&ესახებ"

#: discover/qml/DiscoverWindow.qml:104
#, kde-format
msgid "S&ettings"
msgstr "&პარამეტრები"

#: discover/qml/DiscoverWindow.qml:157 discover/qml/DiscoverWindow.qml:338
#: discover/qml/DiscoverWindow.qml:445
#, kde-format
msgid "Error"
msgstr "შეცდომა"

#: discover/qml/DiscoverWindow.qml:161
#, kde-format
msgid "Unable to find resource: %1"
msgstr "რესურსის პოვნა შეუძლებელია: %1"

#: discover/qml/DiscoverWindow.qml:256 discover/qml/SourcesPage.qml:180
#, kde-format
msgid "Proceed"
msgstr "გაგრძელება"

#: discover/qml/DiscoverWindow.qml:314
#, kde-format
msgid "Report this issue"
msgstr "ამ პრობლემის რეპორტი"

#: discover/qml/DiscoverWindow.qml:338
#, kde-format
msgid "Error %1 of %2"
msgstr "შეცდომა %1 %2-დან"

#: discover/qml/DiscoverWindow.qml:383
#, kde-format
msgctxt "@action:button"
msgid "Show Previous"
msgstr "წინას ჩვენება"

#: discover/qml/DiscoverWindow.qml:396
#, kde-format
msgctxt "@action:button"
msgid "Show Next"
msgstr "შემდეგის ჩვენება"

#: discover/qml/DiscoverWindow.qml:412 discover/qml/UpdatesPage.qml:98
#, kde-format
msgid "Copy to Clipboard"
msgstr "ბუფერში კოპირება"

#: discover/qml/Feedback.qml:13
#, kde-format
msgid "Submit usage information"
msgstr "გამოყენების მონაცემების გადაცემა"

#: discover/qml/Feedback.qml:14
#, kde-format
msgid ""
"Sends anonymized usage information to KDE so we can better understand our "
"users. For more information see https://kde.org/privacypolicy-apps.php."
msgstr ""
"KDE-სთვის მოხმარების ანონიმური მონაცემების გაგზავნა, რათა ჩვენი "
"მომხმარებლების უკეთ გვესმოდეს. მეტი ინფორმაციისთვის იხილეთ https://kde.org/"
"privacypolicy-apps.php."

#: discover/qml/Feedback.qml:18
#, kde-format
msgid "Submitting usage information…"
msgstr "გამოყენების ინფორმაციის გადაცემა…"

#: discover/qml/Feedback.qml:18
#, kde-format
msgid "Configure"
msgstr "მორგება"

#: discover/qml/Feedback.qml:22
#, kde-format
msgid "Configure feedback…"
msgstr "უკუპასუხის მორგება…"

#: discover/qml/Feedback.qml:29 discover/qml/SourcesPage.qml:21
#, kde-format
msgid "Configure Updates…"
msgstr "განახლებების მორგება…"

#: discover/qml/Feedback.qml:57
#, kde-format
msgid ""
"You can help us improving this application by sharing statistics and "
"participate in surveys."
msgstr ""
"თქვენი სტატისტიკის გაზიარებით შეგიძლიათ აპლიკაციების გაუმჯობესებაში "
"დაგვეხმაროთ."

#: discover/qml/Feedback.qml:57
#, kde-format
msgid "Contribute…"
msgstr "შემოწირულობა…"

#: discover/qml/Feedback.qml:62
#, kde-format
msgid "We are looking for your feedback!"
msgstr "ჩვენ ველოდებით თქვენს გამოხმაურებას!"

#: discover/qml/Feedback.qml:62
#, kde-format
msgid "Participate…"
msgstr "მონაწილეობის მიღება…"

#: discover/qml/InstallApplicationButton.qml:26
#, kde-format
msgctxt "State being fetched"
msgid "Loading…"
msgstr "ჩატვირთვა…"

#: discover/qml/InstallApplicationButton.qml:30
#, kde-format
msgctxt "@action:button %1 is the name of a software repository"
msgid "Install from %1"
msgstr "%1-დან დაყენება"

#: discover/qml/InstallApplicationButton.qml:32
#, kde-format
msgctxt "@action:button"
msgid "Install"
msgstr "დაყენება"

#: discover/qml/InstallApplicationButton.qml:34
#, kde-format
msgid "Remove"
msgstr "წაშლა"

#: discover/qml/InstalledPage.qml:16
#, kde-format
msgid "Installed"
msgstr "ჩადგმულია"

#: discover/qml/navigation.js:18
#, kde-format
msgid "Resources for '%1'"
msgstr "რესურსები '%1'-სთვის"

#: discover/qml/ProgressView.qml:17
#, kde-format
msgid "Tasks (%1%)"
msgstr "ამოცანა (%1%)"

#: discover/qml/ProgressView.qml:17 discover/qml/ProgressView.qml:42
#, kde-format
msgid "Tasks"
msgstr "ამოცანები"

#: discover/qml/ProgressView.qml:102
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed, remaining time"
msgid "%1 - %2: %3, %4 remaining"
msgstr "%1 - %2: %3, %4 დარჩენილია"

#: discover/qml/ProgressView.qml:103
#, kde-format
msgctxt "TransactioName - TransactionStatus: speed"
msgid "%1 - %2: %3"
msgstr "%1 - %2: %3"

#: discover/qml/ProgressView.qml:104
#, kde-format
msgctxt "TransactioName - TransactionStatus"
msgid "%1 - %2"
msgstr "%1 - %2"

#: discover/qml/ReviewDelegate.qml:64
#, kde-format
msgid "unknown reviewer"
msgstr "უცნობი მიმომხილველი"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "<b>%1</b> by %2"
msgstr "<b>%1</b> by %2"

#: discover/qml/ReviewDelegate.qml:65
#, kde-format
msgid "Comment by %1"
msgstr "%1-ის კომენტარი"

#: discover/qml/ReviewDelegate.qml:83
#, kde-format
msgid "Version: %1"
msgstr "ვერსია: %1"

#: discover/qml/ReviewDelegate.qml:83
#, kde-format
msgid "Version: unknown"
msgstr "უცნობი ვერსია"

#: discover/qml/ReviewDelegate.qml:98
#, kde-format
msgid "Votes: %1 out of %2"
msgstr "ხმები: %1-ი %2-დან"

#: discover/qml/ReviewDelegate.qml:105
#, kde-format
msgid "Was this review useful?"
msgstr "მიმოხილვა სასარგებლო იყო?"

#: discover/qml/ReviewDelegate.qml:117
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "Yes"
msgstr "დიახ"

#: discover/qml/ReviewDelegate.qml:134
#, kde-format
msgctxt "Keep this string as short as humanly possible"
msgid "No"
msgstr "არა"

#: discover/qml/ReviewDialog.qml:19
#, kde-format
msgid "Reviewing %1"
msgstr "განხილვა: %1"

#: discover/qml/ReviewDialog.qml:25
#, kde-format
msgid "Submit review"
msgstr "მიმოხილვის დამტკიცება"

#: discover/qml/ReviewDialog.qml:38
#, kde-format
msgid "Rating:"
msgstr "შეფასება:"

#: discover/qml/ReviewDialog.qml:43
#, kde-format
msgid "Name:"
msgstr "სახელი:"

#: discover/qml/ReviewDialog.qml:51
#, kde-format
msgid "Title:"
msgstr "სათაური:"

#: discover/qml/ReviewDialog.qml:69
#, kde-format
msgid "Enter a rating"
msgstr "შეიყვანეთ შეფასება"

#: discover/qml/ReviewDialog.qml:72
#, kde-format
msgid "Write the title"
msgstr "დაწერეთ სათაური"

#: discover/qml/ReviewDialog.qml:75
#, kde-format
msgid "Write the review"
msgstr "დაწერეთ შეფასება"

#: discover/qml/ReviewDialog.qml:78
#, kde-format
msgid "Keep writing…"
msgstr "გააგრძელეთ წერა…"

#: discover/qml/ReviewDialog.qml:81
#, kde-format
msgid "Too long!"
msgstr "ძალიან გრძელია!"

#: discover/qml/ReviewDialog.qml:84
#, kde-format
msgctxt "@info:usagetip"
msgid "Insert a name"
msgstr "სახელის ჩასმა"

#: discover/qml/ReviewsPage.qml:44
#, kde-format
msgid "Reviews for %1"
msgstr "%1-ის შეფასებები"

#: discover/qml/ReviewsPage.qml:55
#, kde-format
msgid "Write a Review…"
msgstr "შეფასების დაწერა…"

#: discover/qml/ReviewsPage.qml:60
#, kde-format
msgid "Install this app to write a review"
msgstr "მიმოხილვის დასაწერად ეს აპი დააყენეთ"

#: discover/qml/SearchField.qml:24
#, kde-format
msgid "Search…"
msgstr "ძებნა…"

#: discover/qml/SearchField.qml:24
#, kde-format
msgid "Search in '%1'…"
msgstr "'%1'-ში ძებნა…"

#: discover/qml/SourcesPage.qml:17
#, kde-format
msgid "Settings"
msgstr "მორგება"

#: discover/qml/SourcesPage.qml:98
#, kde-format
msgid "Default source"
msgstr "ნაგულისხმები წყარო"

#: discover/qml/SourcesPage.qml:105
#, kde-format
msgid "Add Source…"
msgstr "წყაროს დამატება…"

#: discover/qml/SourcesPage.qml:131
#, kde-format
msgid "Make default"
msgstr "ნაგულისხმებად გამოყენება"

#: discover/qml/SourcesPage.qml:222
#, kde-format
msgid "Increase priority"
msgstr "პრიორიტეტის გაზრდა"

#: discover/qml/SourcesPage.qml:228
#, kde-format
msgid "Failed to increase '%1' preference"
msgstr "'%1'-ის უპირატესობის გაზრდის შეცდომა"

#: discover/qml/SourcesPage.qml:234
#, kde-format
msgid "Decrease priority"
msgstr "პრიორიტეტის დაწევა"

#: discover/qml/SourcesPage.qml:240
#, kde-format
msgid "Failed to decrease '%1' preference"
msgstr "'%1'-ის უპირატესობის შემცირების შეცდომა"

#: discover/qml/SourcesPage.qml:246
#, kde-format
msgid "Remove repository"
msgstr "რეპოზიტორიის წაშლა"

#: discover/qml/SourcesPage.qml:257
#, kde-format
msgid "Show contents"
msgstr "შემცველობის ჩვენება"

#: discover/qml/SourcesPage.qml:297
#, kde-format
msgid "Missing Backends"
msgstr "უკანაბოლოები ხელმიუწვდომელია"

#: discover/qml/UpdatesPage.qml:12
#, kde-format
msgid "Updates"
msgstr "განახლებები"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Update Issue"
msgstr "განახლების პრობლემა"

#: discover/qml/UpdatesPage.qml:45
#, kde-format
msgid "Technical details"
msgstr "ტექნიკური დეტალები"

#: discover/qml/UpdatesPage.qml:61
#, kde-format
msgid "There was an issue installing this update. Please try again later."
msgstr "პრობლემა ამ განახლების დაყენებისას. მოგვიანებით სცადეთ."

#: discover/qml/UpdatesPage.qml:67
#, kde-format
msgid "See Technical Details"
msgstr "იხილეთ ტექნიკური დეტალები"

#: discover/qml/UpdatesPage.qml:83
#, kde-format
msgid ""
"If you would like to report the update issue to your distribution's "
"packagers, include this information:"
msgstr ""
"თუ გნებავთ, რომ თქვენი დისტრიბუტივის პაკეტების შეცდომის შესახებ ანგარიში "
"გააგზავნოთ, ჩასვით ეს ინფორმაციაც:"

#: discover/qml/UpdatesPage.qml:102
#, kde-format
msgid "Error message copied to clipboard"
msgstr "შეცდომის შეტყობინება გაცვლის ბუფერში დაკოპირდა"

#: discover/qml/UpdatesPage.qml:134
#, kde-format
msgctxt "@action:button as in, 'update all items'"
msgid "Update All"
msgstr "ყველას განახლება"

#: discover/qml/UpdatesPage.qml:174
#, kde-format
msgid "Ignore"
msgstr "იგნორი"

#: discover/qml/UpdatesPage.qml:220
#, kde-format
msgid "Select All"
msgstr "ყველას მონიშვნა"

#: discover/qml/UpdatesPage.qml:228
#, kde-format
msgid "Select None"
msgstr "მონიშვნის მოხსნა"

#: discover/qml/UpdatesPage.qml:235
#, kde-format
msgid "Restart automatically after update has completed"
msgstr "ავტომატური გადატვირთვა განახლების შემდეგ"

#: discover/qml/UpdatesPage.qml:242
#, kde-format
msgid "Total size: %1"
msgstr "ჯამური ზომა: %1"

#: discover/qml/UpdatesPage.qml:277
#, kde-format
msgid "Restart Now"
msgstr "ახლა გადატვირთვა"

#: discover/qml/UpdatesPage.qml:376
#, kde-format
msgid "%1"
msgstr "%1"

#: discover/qml/UpdatesPage.qml:392
#, kde-format
msgid "Installing"
msgstr "დაყენება"

#: discover/qml/UpdatesPage.qml:428
#, kde-format
msgid "Update from:"
msgstr "განახლების წყარო:"

#: discover/qml/UpdatesPage.qml:440
#, kde-format
msgctxt ""
"%1 is the backend that provides this app, %2 is the specific repository or "
"address within that backend"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: discover/qml/UpdatesPage.qml:447
#, kde-format
msgid "More Information…"
msgstr "მეტი ინფორმაცია…"

#: discover/qml/UpdatesPage.qml:475
#, kde-format
msgctxt "@info"
msgid "Fetching updates…"
msgstr "განახლებების გამოთხოვა…"

#: discover/qml/UpdatesPage.qml:488
#, kde-format
msgctxt "@info"
msgid "Updates"
msgstr "განახლებები"

#: discover/qml/UpdatesPage.qml:498
#, kde-format
msgctxt "@info"
msgid "Restart the system to complete the update process"
msgstr "განახლების დასასრულებლად გადატვირთეთ კომპიუტერი"

#: discover/qml/UpdatesPage.qml:510 discover/qml/UpdatesPage.qml:517
#: discover/qml/UpdatesPage.qml:524
#, kde-format
msgctxt "@info"
msgid "Up to date"
msgstr "განახლებულია"

#: discover/qml/UpdatesPage.qml:531
#, kde-format
msgctxt "@info"
msgid "Should check for updates"
msgstr "უნდა შემოწმდეს განახლებები, თუ არა"

#: discover/qml/UpdatesPage.qml:538
#, kde-format
msgctxt "@info"
msgid "Time of last update unknown"
msgstr "ბოლო განახლების თარიღი უცნობია"

#~ msgid "Update Selected"
#~ msgstr "მონიშნულის განახლება"

#~ msgid "Sources"
#~ msgstr "წყაროები"

#~ msgid "Distributed by"
#~ msgstr "დისტრიბუტორი"

#~ msgid "Featured"
#~ msgstr "პოპულარული"

#~ msgid "Updates are available"
#~ msgstr "ხელმისაწვდომია განახლებები"

#~ msgctxt "Short for 'show updates'"
#~ msgid "Show"
#~ msgstr "ჩვენება"

#~ msgid "proprietary"
#~ msgstr "დახურული კოდი"
